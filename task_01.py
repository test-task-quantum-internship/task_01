import time

def sum_of_numbers(n):
    start = time.time() # start the timer
    sum = (n * (n + 1)) // 2 # calculate the sum of numbers from 1 to N using formula
    end = time.time() # stop the timer
    print("Execution Time:", end - start) # print the execution time
    return sum

n = int(input("Enter a positive integer: "))
print("Sum of numbers from 1 to", n, "is", sum_of_numbers(n))