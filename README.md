# Task 01: What is FOR loop?
## Task:
You have a positive integer number N as an input. Please write a program in Python 3 that calculates the sum in range 1 and N.

Limitations:
- N <= 10^25;
- Execution time: 0.1 seconds.

Examples:
- Input: 1
- Output: 1

- Input: 3
- Output: 6

- Input 10:
- Output: 55

## Solution:

```py
import time

def sum_of_numbers(n):
    start = time.time() # start the timer
    sum = (n * (n + 1)) // 2 # calculate the sum of numbers from 1 to N using formula
    end = time.time() # stop the timer
    print("Execution Time:", end - start) # print the execution time
    return sum

n = int(input("Enter a positive integer: "))
print("Sum of numbers from 1 to", n, "is", sum_of_numbers(n))
```

Explanation:

The function `sum_of_numbers` takes a positive integer `n` as input and calculates the sum of numbers from 1 to `n` using the formula `(n * (n + 1)) // 2`. The `//` operator is used for integer division to get rid of any fractional part.

The program also measures the execution time using the `time` module. The `start` variable is set to the current time before executing the function, and the `end` variable is set to the current time after executing the function. The difference between the `end` and `start` time gives the execution time, which is printed to the console.

Finally, the program asks the user to enter a positive integer `n`, calls the `sum_of_numbers` function, and prints the result.